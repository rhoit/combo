# combo

### `n!/{k!(n-k)!}`

Show all possible combinations of the string

     i.e. "abc" 6 combinations => abc,acb,bac,bca...
 

#### HOW-TO USE
     
     $ make
     $ ./combo

